# coding: utf-8
import os
import random
import sys
from biplist import *
import chardet
import re
from datetime import datetime
import shutil
import io


unzip_temp_path = ''


# 检查输入的ipa格式
def check_ipa_path(ipa_path):
    file_name, file_type = os.path.splitext(ipa_path)
    if file_type == ".ipa":
        return True
    else:
        return False


# 检查齿轮文件
def check_mobile_provision_path(mobile_provision_path):
    file_name, file_type = os.path.splitext(mobile_provision_path)
    if file_type == ".mobileprovision":
        return True
    else:
        return False


# 生成随机字符串
def get_random_string(length=10):
    seed = "_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    sa = []
    for i in range(length):
        sa.append(random.choice(seed))
        salt = ''.join(sa)
    return salt


# 解压
def unzip_ipa(ipa_path, target_path):
    dir_path = os.path.dirname(ipa_path)
    temp_path = target_path
    if os.path.exists(temp_path):
        temp_path = os.path.join(dir_path, get_random_string(20))
    if os.path.exists(temp_path):
        temp_path = os.path.join(dir_path, get_random_string(20))
    if os.path.exists(temp_path):
        sys.exit("创建文件夹缓存文件夹失败")
    else:
        os.mkdir(temp_path)
    command_string = "/usr/bin/unzip -q %s -d %s" % (ipa_path, temp_path)
    print("命令_解压" + command_string)
    os.system(command_string)
    return temp_path


# 获取.app文件路径
def get_app_path_with_unzip_path(unzip_path):
    for dir_path, sub_paths, files in os.walk(unzip_path, False):
        if dir_path.endswith(".app"):
            return dir_path
    return ""


# 复制文件
def copy_file(file_path, target_path):
    if os.path.exists(target_path):
        os.remove(target_path)
    command_string = "/bin/cp -r %s %s" % (file_path, target_path)
    print("命令_复制:" + command_string)
    os.system(command_string)
    if not os.path.exists(target_path):
        command_string_old_system = "cp -R %s %s" % (file_path, target_path)
        os.system(command_string_old_system)


# 标准齿轮文件名
def standard_mobileprovision_name():
    return "embedded.mobileprovision"


# 标准Info.plist文件名
def standard_plist_name():
    return "Info.plist"


# 更改bundleID
def change_bundle_id(original_plist_path, target_bundle_id_string):
    plist_info = {}
    try:
        plist_info = readPlist(original_plist_path)
    except ZeroDivisionError as err:  # as 加原因参数名称
        print('Read Plist Exception: ', err)
    print(plist_info["CFBundleIdentifier"])
    plist_info["CFBundleIdentifier"] = str(target_bundle_id_string)
    try:
        writePlist(plist_info, original_plist_path)
    except ZeroDivisionError as err:
        print("WritePlistError: ", err)


# 生成entitlements.plist
def create_entitlements_plist(unzip_temp_path, mobile_provision_path):
    profile_path = os.path.join(unzip_temp_path, "profile.plist")
    command_string = "/usr/bin/security cms -D -i %s > %s" % (mobile_provision_path, profile_path)
    print("命令_制作entitlements.plist文件:" + command_string)
    os.system(command_string)
    temp_plist = {}
    try:
        temp_plist = readPlist(profile_path)
    except ZeroDivisionError as err:
        print('Read Plist Exception: ', err)
    entitlements_path = os.path.join(unzip_temp_path, "entitlements.plist")
    entitlements_list = temp_plist["Entitlements"]
    target_bundle_id_line = entitlements_list["application-identifier"]
    target_bundle_id_string = target_bundle_id_line.split(".", 1)[-1]
    write_entitlements_commad_string = "/usr/libexec/PlistBuddy -x -c 'Print :Entitlements' %s > %s" %(profile_path, entitlements_path)
    os.system(write_entitlements_commad_string)
    return entitlements_path, target_bundle_id_string


def codesign_app(certificate_name, entitlements_path, app_path):
    command_string = "/usr/bin/codesign -fs \"%s\" --entitlements=%s %s" % (
        certificate_name,
        entitlements_path,
        app_path
                                                                       )
    print("命令_生成entitlements.plist:" + command_string)
    os.system(command_string)


def codesign_framework(certificate_name, app_path):
    has_framework = False
    has_dylib = False
    for dir_path, sub_paths, files in os.walk(app_path, False):
        is_continue = True
        if not is_continue:
            break
        for s_file in files:
            file_name, file_type = os.path.splitext(s_file)
            if file_type == ".framework":
                has_framework = True
                is_continue = False
                break
    for dir_path, sub_paths, files in os.walk(app_path, False):
        is_continue = True
        if not is_continue:
            break
        for s_file in files:
            file_name, file_type = os.path.splitext(s_file)
            if file_type == ".dylib":
                is_continue = False
                has_dylib = True
                break

    if has_framework:
        command_string = "/usr/bin/codesign -fs %s %s" % (
            certificate_name,
            os.path.join(app_path, "**/*.framework")
        )
        print("命令_签名framework:" + command_string)
        os.system(command_string)
    if has_dylib:
        command_string = "/usr/bin/codesign -fs %s %s" % (
            certificate_name,
            os.path.join(app_path, "**/*.dylib")
        )
        print("命令_签名dylib:" + command_string)
        os.system(command_string)


def search_local_certificate(unzip_path):
    certificate_path = os.path.join(unzip_path, "certificate.txt")
    command_string = "security find-identity -p codesigning -v > %s" % certificate_path
    print("命令_遍历证书:" + command_string)
    os.system(command_string)
    f = open(certificate_path, 'rb')
    data = f.read()
    encode_type = chardet.detect(data)["encoding"]
    f.close()
    file_content_list = []
    file_object_1 = io.open(certificate_path, "rb+")
    file_content_list = file_object_1.readlines()
    file_object_1.close()
    certificate_dic = {}
    certificate_pattern = re.compile(r'.*(iPhone|Mac).*(Developer|Distribution).*')
    del_index_list = []
    for value in file_content_list:
        certificate_value = str(value)
        certificate_value = certificate_value.strip()
        match = certificate_pattern.match(certificate_value)
        if not match:
            del_index_list.append(file_content_list.index(value))
            continue
        key = certificate_value.split(") ", 1)[0]
        certificate_name = certificate_value.split("\"")[1]
        certificate_dic[key] = certificate_name
    for index in del_index_list:
        del file_content_list[index]
    return file_content_list, certificate_dic


# 生成重签名ipa包
def create_resign_ipa(unzip_path, original_ipa_path):
    pay_load_path = "./Payload"
    dir_path, s_file = os.path.split(original_ipa_path)
    file_name, file_type = os.path.splitext(s_file)
    time_now = str(datetime.now())
    time_now = time_now.replace(":", "_")
    time_now = time_now.replace(" ", "_")
    time_now = time_now.replace("-", "_")
    resign_file_name = file_name + "_resign" + time_now + file_type
    resign_file_path = os.path.join(dir_path, resign_file_name)
    zip_command_string = '/usr/bin/zip -qr %s %s' % (resign_file_path, pay_load_path)
    print("命令_生成重签名ipa：" + zip_command_string)
    cd_command = "cd %s" % unzip_path
    print("命令_cd到文件夹：" + cd_command)
    os.system(cd_command + "&&" + zip_command_string)
    return resign_file_path


# 重命名.app文件，防止有空格
def rename_app_file(app_path):
    dir_path = os.path.dirname(app_path)
    new_app_file_name = get_random_string() + ".app"
    new_app_path = os.path.join(dir_path, new_app_file_name)
    os.renames(app_path, new_app_path)


# 验证重签名包
def check_resign_resulet(app_path):
    check_command_string = '/usr/bin/codesign -v %s' % app_path
    is_success = os.system(check_command_string)
    if is_success == 0:
        return True
    else:
        return False


def resign():
    global unzip_temp_path
    original_ipa_path = raw_input("输入ipa路径或拖ipa文件至命令行:\n")
    original_ipa_path = original_ipa_path.strip()
    if not check_ipa_path(original_ipa_path):
        print("ipa文件类型错误")
        return
    mobile_provision_path = raw_input("输入mobileprovision文件路径:\n")
    mobile_provision_path = mobile_provision_path.strip()
    if not check_mobile_provision_path(mobile_provision_path):
        print("mobileprovision文件类型错误")
        return
    print("ipa路径:\n" + original_ipa_path)
    print("mobile_provision路径:\n" + mobile_provision_path)
    dir_path = os.path.dirname(original_ipa_path)
    unzip_temp_path = os.path.join(dir_path, "resign_temp")
    unzip_temp_path = unzip_ipa(original_ipa_path, unzip_temp_path)
    app_path = get_app_path_with_unzip_path(unzip_temp_path)
    rename_app_file(app_path=app_path)
    app_path = get_app_path_with_unzip_path(unzip_temp_path)
    if app_path == "":
        sys.exit("解压失败")
    original_mobileprovision_path = os.path.join(app_path, standard_mobileprovision_name())
    copy_file(mobile_provision_path, original_mobileprovision_path)
    print("复制齿轮文件完成")
    original_plist_path = os.path.join(app_path, standard_plist_name())
    entitlements_path, target_bundle_id_string = create_entitlements_plist(unzip_temp_path, mobile_provision_path)
    # change_bundle_id(original_plist_path=original_plist_path,target_bundle_id_string=target_bundle_id_string)
    file_content_list, certificate_dic = search_local_certificate(unzip_temp_path)
    print("本机证书列表：\n")
    for certificate_line in file_content_list:
        print(certificate_line)
    index = raw_input("输入签名证书编号:\n")
    index_pattern = re.compile(r'[0-9]$')
    match = index_pattern.match(index)
    if not match:
        shutil.rmtree(unzip_temp_path)
        sys.exit("输入编号错误，请输入正确的数字编号，例：1")
    entitlements_name = certificate_dic[index]
    codesign_framework(certificate_name=entitlements_name, app_path=app_path)
    codesign_app(certificate_name=entitlements_name, entitlements_path=entitlements_path, app_path=app_path)
    print("正在验证...")
    is_resign_success = check_resign_resulet(app_path=app_path)
    if is_resign_success:
        print("重签名成功！")
    else:
        shutil.rmtree(unzip_temp_path)
        sys.exit("重签名数据包签名验证失败！")
    print("正在生成重签名ipa包...")
    resign_file_path = create_resign_ipa(unzip_path=unzip_temp_path, original_ipa_path=original_ipa_path)
    is_exist_resign_file = os.path.exists(resign_file_path)
    if is_exist_resign_file:
        print("生成重签名ipa成功：" + resign_file_path)
        shutil.rmtree(unzip_temp_path)
    else:
        shutil.rmtree(unzip_temp_path)
        sys.exit("生成重签名ipa失败")


resign()
