##  重签名脚本说明文档
#### 重要提示：此脚本必须在mac上运行!
#### 支持包括企业包的各种ipa包重签名
#### 1、安装python运行环境, python2环境对应后缀python2的重签名脚本,python3对应后缀python3, python3有对应的字节码文件可用。
#### 2、安装python第三方库管理工具pip
#### 3、使用pip安装脚本依赖的第三方库：biplist、chardet、coverage, 例:pip install biplist,安装了多版本python使用pip2/pip3安装对应环境库,安装出错就使用管理员权限重新安装,例:sudo pip install biplist
#### 4、运行脚本根据提示拖入ipa和齿轮文件，脚本会读取本地钥匙串证书列表，输入相应的证书编号即可完成
